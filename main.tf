# ******************************************************************************************************************
# *  main.tf                                                                                                       *
# ******************************************************************************************************************
# *                                                                                                                *
# *  Project: Migration Project                                                                                    *
# *                                                                                                                *
# *  Copyright © 2021 Codeetester. All Rights Reserved.                                                            *
# *                                                                                                                *
# *  This module implements the deployment of AWS DMS resources for a db migration from Oracle to RDS PostgreSQL.  *                                                                                             *
# *                                                                                                                *
# *  The following resources are created:                                                                          *
# *                                                                                                                *
# *   1) Three AWS IAM Roles required for the creation of DMS replication instance (Item 4) below:                 *
# *      a) DMS Endpoint Access Role.                                                                              *
# *      b) DMS Cloudwatch Logs Role.                                                                              *
# *      c) DMS VPC Role.                                                                                          *
# *                                                                                                                *
# *   2) DMS Source Endpoint.                                                                                      *
# *                                                                                                                *
# *   3) DMS Target Endpoint.                                                                                      *
# *                                                                                                                *
# *   4) DMS Replicating Instance.                                                                                 *
# *                                                                                                                *
# *   5) DMS Migrating task                                                                                        *
# *                                                                                                                *
# ******************************************************************************************************************



# configure provider(s) and backend
terraform {

  required_providers {
    aws = {
      source = "hashicorp/aws"
    }

    random = {
      source = "hashicorp/random"
    }
  }

  backend "local" {

  }
  
  /*
  backend "s3" {
    bucket  = "my-bucket-name"
    key     = "dms/terraform.tfstate"
    region  = "us-east-1"
    encrypt = true
  }
  */

}

# local variables
locals {
 aws_tags = {
    "environment" = var.environment
    "map-dba"    = var.map_dba
 }
}


# 0. create uuid, to be appended to the IAM role names (uuid 4-element substring), to ensure uniqueness
resource "random_uuid" "secret_random_uuid" { }


# 1. create dms access role - required for the creation of DMS replicating instance
resource "aws_iam_role" "dms_endpoint_access_role" {
  name = "${var.org_identifier}-${var.environment}-${var.dms_endpoint_access_role_name}-${substr(random_uuid.secret_random_uuid.result, 0, 4)}"
  path = "/"
  managed_policy_arns = ["arn:aws:iam::aws:policy/service-role/AmazonDMSRedshiftS3Role"]
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action    = "sts:AssumeRole"
        Effect    = "Allow"
        Sid       = "AllowDMSRedshiftS3ToAssumeRole"
        Principal = {
          Service = "dms.amazonaws.com"
        }
      },
    ]
  })
}

# 2.  create dms cloudwatch logs role - required for the creation of DMS replicating instance
resource "aws_iam_role" "dms_cloudwatch_logs_role" {
  name = "${var.org_identifier}-${var.environment}-${var.dms_cloudwatch_logs_role_name}-${substr(random_uuid.secret_random_uuid.result, 0, 4)}"
  path = "/"
  managed_policy_arns = ["arn:aws:iam::aws:policy/service-role/AmazonDMSCloudWatchLogsRole"]
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action    = "sts:AssumeRole"
        Effect    = "Allow"
        Sid       = "AllowDMSCloudWatchLogsToAssumeRole"
        Principal = {
          Service = "dms.amazonaws.com"
        }
      },
    ]
  })
}

# 3.  dms-cloudwatch logs role - required for the creation of DMS replicating instance
resource "aws_iam_role" "dms_vpc_role" {
  name = "${var.org_identifier}-${var.environment}-${var.dms_vpc_role_name}-${substr(random_uuid.secret_random_uuid.result, 0, 4)}"
  path = "/"
  managed_policy_arns = ["arn:aws:iam::aws:policy/service-role/AmazonDMSVPCManagementRole"]
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action    = "sts:AssumeRole"
        Effect    = "Allow"
        Sid       = "AllowDMSVPCManagementToAssumeRole"
        Principal = {
          Service = "dms.amazonaws.com"
        }
      },
    ]
  })
}

# 4. create a new replication subnet group
resource "aws_dms_replication_subnet_group" "dms_replication_subnet_group" {
  replication_subnet_group_description = var.replication_subnet_group_description
  replication_subnet_group_id          = "${var.org_identifier}-${var.environment}-${var.replication_subnet_group_id}"
  subnet_ids                           = var.replication_subnet_ids

  tags = merge(
    {
        name                           = "${var.org_identifier}-${var.environment}-${var.replication_subnet_group_id}"
    },
    {
        creator                        = var.creator
    },
    local.aws_tags
  )
}

# 5. create a source endpoint
resource "aws_dms_endpoint" "dms_source_endpoint" {
  database_name               = var.source_database_name
  endpoint_id                 = "${var.org_identifier}-${var.environment}-${var.source_endpoint_id}"
  endpoint_type               = var.source_endpoint_type
  engine_name                 = var.source_engine_name
  extra_connection_attributes = var.source_extra_connection_attributes
  password                    = var.source_password
  username                    = var.source_username
  port                        = var.source_port
  server_name                 = var.source_server_name
  ssl_mode                    = var.source_ssl_mode

  tags = merge(
    {
        name                  = "${var.org_identifier}-${var.environment}-${var.source_endpoint_id}"
    },
    {
        creator               = var.creator
    },
    local.aws_tags
  )
}

# 6. create a target endpoint
resource "aws_dms_endpoint" "dms_target_endpoint" {
  database_name               = var.target_database_name
  endpoint_id                 = "${var.org_identifier}-${var.environment}-${var.target_endpoint_id}"
  endpoint_type               = var.target_endpoint_type
  engine_name                 = var.target_engine_name
  extra_connection_attributes = var.target_extra_connection_attributes
  password                    = var.target_password
  username                    = var.target_username
  port                        = var.target_port
  server_name                 = var.target_server_name
  ssl_mode                    = var.target_ssl_mode
  
  tags = merge(
    {
        name                  = "${var.org_identifier}-${var.environment}-${var.target_endpoint_id}"
    },
    {
        creator               = var.creator
    },
    local.aws_tags
  )
}

# 7. create a replication instance
resource "aws_dms_replication_instance" "dms_replication_instance" {
  depends_on                   = [aws_iam_role.dms_endpoint_access_role,
                                  aws_iam_role.dms_cloudwatch_logs_role,
                                  aws_iam_role.dms_vpc_role
                                 ]
  allocated_storage            = var.replication_instance_allocated_storage
  apply_immediately            = var.replication_instance_apply_immediately
  auto_minor_version_upgrade   = var.replication_instance_auto_minor_version_upgrade
  availability_zone            = var.replication_instance_availability_zone
  engine_version               = var.replication_instance_engine_version
  multi_az                     = var.replication_instance_multi_az
  preferred_maintenance_window = var.replication_instance_preferred_maintenance_window
  publicly_accessible          = var.replication_instance_publicly_accessible
  replication_instance_class   = var.replication_instance_class
  replication_subnet_group_id  = aws_dms_replication_subnet_group.dms_replication_subnet_group.id
  replication_instance_id      = "${var.org_identifier}-${var.environment}-${var.replication_instance_id}"
  
  tags = merge(
    {
        name                   = "${var.org_identifier}-${var.environment}-${var.replication_instance_id}"
    },
    {
        creator                = var.creator
    },
    local.aws_tags
  )
}

# 8. create a replication task
resource "aws_dms_replication_task" "dms_replication_task" {
  depends_on                  = [aws_dms_endpoint.dms_target_endpoint,
                                 aws_dms_endpoint.dms_source_endpoint,
                                 aws_dms_replication_instance.dms_replication_instance]
  migration_type              = var.migration_type
  replication_task_settings   = jsonencode({ "TargetMetadata": { "TaskRecoveryTableEnabled": var.task_recovery_table_enabled}, "ValidationSettings": { "EnableValidation": var.enable_validation} })
  replication_instance_arn    = aws_dms_replication_instance.dms_replication_instance.replication_instance_arn
  replication_task_id         = "${var.org_identifier}-${var.environment}-${var.replication_task_id}"
  source_endpoint_arn         = aws_dms_endpoint.dms_source_endpoint.endpoint_arn
  target_endpoint_arn         = aws_dms_endpoint.dms_target_endpoint.endpoint_arn
  table_mappings              = var.table_mappings

  tags = merge(
    {
        name                  = "${var.org_identifier}-${var.environment}-${var.replication_task_id}"
    },
    {
        creator               = var.creator
    },
    local.aws_tags
  )
}