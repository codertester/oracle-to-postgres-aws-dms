# ******************************************************************************************************************
# *  outputs.tf                                                                                                    *
# ******************************************************************************************************************
# *                                                                                                                *
# *  Project: Migration Project                                                                                    *
# *                                                                                                                *
# *  Copyright © 2021 Codetester. All Rights Reserved.                                                             *
# *                                                                                                                *
# ******************************************************************************************************************


# 1.
output "dms_endpoint_access_role_arn" {
  description = "ARN of the created dms endpoint access role."
  value = aws_iam_role.dms_endpoint_access_role.arn
}

# 2.
output "dms_cloudwatch_logs_role_arn" {
  description = "ARN of the created dms cloudwatch logs role."
  value = aws_iam_role.dms_cloudwatch_logs_role.arn
}

# 3.
output "dms_vpc_role_arn" {
  description = "ARN of the created dms vpc role."
  value = aws_iam_role.dms_vpc_role.arn
}

# 4.
output "dms_replication_subnet_group_attributes" {
  description = "key-value pair attributes of the created replication subnet group."
  value = aws_dms_replication_subnet_group.dms_replication_subnet_group
}

# 5.
output "dms_source_endpoint_attributes" {
  description = "key-value pair attributes of the created source endpoint."
  value = aws_dms_endpoint.dms_source_endpoint
  sensitive = true
}

# 6.
output "dms_target_endpoint_attributes" {
  description = "key-value pair attributes of the created target endpoint."
  value = aws_dms_endpoint.dms_target_endpoint
  sensitive = true
}

# 7.
output "aws_dms_replication_instance_attributes" {
  description = "key-value pair attributes of the created replication instance"
  value = aws_dms_replication_instance.dms_replication_instance
}

# 8.
output "aws_dms_replication_task_attributes" {
  description = "key-value pair attributes of the created replication task."
  value = aws_dms_replication_task.dms_replication_task
}