# ******************************************************************************************************************
# *  test.auto.tfvars                                                                                              *
# ******************************************************************************************************************
# *                                                                                                                *
# *  Project: Migration Project                                                                                    *
# *                                                                                                                *
# *  Copyright © 2021 Codetester. All Rights Reserved.                           								   *
# *                                                                                                                *
# *                                                                                                                *
# ******************************************************************************************************************


# 0. organization identifier, project,creator  environmental and tagging variables
org_identifier                                      = "org"
project_name                                        = "mgr"
region                                              = "us-east-1"
creator                                             = "Terraform"
environment                                         = "test"
map_dba												= "map-dba"


# 1. create dms access role variabl(s)
dms_endpoint_access_role_name                       = "dms-endpoint-access-role"

# 2. create dms cloudwatch logs role variable(3)
dms_cloudwatch_logs_role_name                       = "dms-cloudwatchlogs-role"

# 3.  create dms-cloudwatch logs role variabl(s)
dms_vpc_role_name                                   = "dms-vpc-role"

# 4. replication subnet group variables
replication_subnet_group_description                = "Org Replication Subnet Group"
replication_subnet_group_id                         = "repl-subnet-group"
replication_subnet_ids                              = ["subnet-0d141dd23929f0eb3", "subnet-09d9ce03690b79111"] #["my-dev-subnet-id-1", "my-dev-subnet-id-2"]

# 5. source endpoint variables
source_database_name                                = "source-SID"
source_endpoint_id                                  = "oracle-source-endpoint"
source_endpoint_type                                = "source"
source_engine_name                                  = "oracle"
source_extra_connection_attributes                  = "addSupplementalLogging=Y;allowSelectNestedTables=true;failTasksOnLobTruncation=true"
source_password                                     = "source-password"
source_username                                     = "source-username"
source_port                                         = 1521
source_server_name                                  = "source-endpoint-url-or-ip"
source_ssl_mode                                     = "none"

# 6. target endpoint variables
target_database_name                                = "target-SID"
target_endpoint_id                                  = "rds-postgres-target-endpoint"
target_endpoint_type                                = "target"
target_engine_name                                  = "postgres"
target_extra_connection_attributes                  = "" 
target_password                                     = "target-password"
target_username                                     = "target-username"
target_port                                         = 5432
target_server_name                                  = "target-endpoint-url-or-ip"
target_ssl_mode                                     = "none"

# 7. replication instance variables
replication_instance_allocated_storage              = 100
replication_instance_apply_immediately              = true
replication_instance_auto_minor_version_upgrade     = true
replication_instance_availability_zone              = "us-east-1a"
replication_instance_engine_version                 = "3.4.4"
replication_instance_multi_az                       = false
replication_instance_preferred_maintenance_window   = "sun:10:05-sun:12:35"
replication_instance_publicly_accessible            = false
replication_instance_class                          = "dms.t3.micro"
replication_instance_id                             = "repl-instance"

# 8. replication task variables
migration_type                                      = "full-load"
task_recovery_table_enabled                         = true
enable_validation                                   = true
replication_task_id                                 = "full-load-replication-task"
table_mappings                                      = "{\"rules\":[{\"rule-type\":\"selection\",\"rule-id\":\"1\",\"rule-name\":\"1\",\"object-locator\":{\"schema-name\":\"%\",\"table-name\":\"%\"},\"rule-action\":\"include\"}]}"