# README #

This README summaries the contents of this repository.

### What is this repository for? ###

* Deployment of AWS DMS resources for the migration of Oracle to AWS RDS PostgreSQL
* Version 1.0

### Deployed resoures ###
* AWS DMS endpoint access role
* AWS DMS cloudwatch logs Role
* AWS DMS VPC role
* AWS DMS source endpoint
* AWS DMS target endpoint
* AWS DMS replicating instance
* AWS DMS migrating task

### Repo usage ###

* Database migration
* Deployment instructions: use bitbucket-pipeline 
* Deployment yml file: **bitbucket-pipelines.yml** 

### Contribution guidelines ###

* Create branch
* Add codes
* Submit pull request for code review

### Who do I talk to? ###

* Repo owner or admin

--- 
 * [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
