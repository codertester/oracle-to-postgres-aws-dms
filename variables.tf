# ******************************************************************************************************************
# *  variables.tf                                                                                                  *
# ******************************************************************************************************************
# *                                                                                                                *
# *  Project: Migration Project                                                                                    *
# *                                                                                                                *
# *  Copyright © 2021 Codetester. All Rights Reserved.                                                             *
# *                                                                                                                *
# ******************************************************************************************************************



# 0. organization identifier, project, region, creator, environmental, and tagging variables
variable "org_identifier" {
  type = string
}

variable "project_name" {
  type = string
}

variable "region" {
  type = string
}

variable "creator" {
  description = "creator of the resource(s): could be e-mail address of creator or simply Terraform."
  type = string
}

variable "environment" {
  description = "Value to put in the environment slug. Provided by the CICD tool."
  type = string
}


variable "map_dba" {
  type = string
}


# 1. create dms access role variable
variable "dms_endpoint_access_role_name" {
  type = string
}


# 2.  create dms cloudwatch logs role variable
variable "dms_cloudwatch_logs_role_name" {
  type = string
}
  

# 3.  create dms-cloudwatch logs role variable
variable "dms_vpc_role_name" {
  type = string
}


# 4. replication subnet group variables
variable "replication_subnet_group_description" {
  type = string
}

variable "replication_subnet_group_id"{
  type = string
}

variable "replication_subnet_ids" {
  type = list(string)
}


# 5. source endpoint variables
variable "source_database_name" {
  type = string
}

variable "source_endpoint_id" {
  type = string
}

variable "source_endpoint_type" {
  type = string
}

variable "source_engine_name" {
  type = string
}

variable "source_extra_connection_attributes" {
   type = string
}

variable "source_password" {
  type = string
}

variable "source_username" {
  type = string
}

variable "source_port" {
  type = number
}

variable "source_server_name" {
  type = string
}

variable "source_ssl_mode" {
  type = string
}


# 6. target endpoint variables
variable "target_database_name" {
  type = string
}

variable "target_endpoint_id" {
  type = string
}

variable "target_endpoint_type" {
  type = string
}

variable "target_engine_name" {
  type = string
}

variable "target_extra_connection_attributes" {
   type = string
}

variable "target_password" {
  type = string
}

variable "target_username" {
  type = string
}

variable "target_port" {
  type = number
}

variable "target_server_name" {
  type = string
}

variable "target_ssl_mode" {
  type = string
}


# 7. replication instance variables
variable "replication_instance_allocated_storage" {
  type = number
}

variable "replication_instance_apply_immediately" {
  type = bool
}

variable "replication_instance_auto_minor_version_upgrade" {
  type = bool
}

variable "replication_instance_availability_zone" {
}

variable "replication_instance_engine_version" {
  type = string
}

variable "replication_instance_multi_az" {
  type = bool
}

variable "replication_instance_preferred_maintenance_window" {
  type = string
}

variable "replication_instance_publicly_accessible" {
  type = bool
}

variable "replication_instance_class" {
  type = string
}

variable "replication_instance_id" {
  type = string
}


# 8. replication task variables
variable "migration_type" {
  type = string
}

variable "task_recovery_table_enabled" {
  type = bool
}

variable "enable_validation" {
  type = bool
}

variable "replication_task_id" {
  type = string
}

variable "table_mappings" {
  type = string
}